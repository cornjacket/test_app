package app

import (
        "log"
        "bitbucket.org/cornjacket/test_app/app/controllers"
)

type AppService struct {
	Context		*controllers.AppContext
} 

func NewAppService() AppService {
	a := AppService{}
	a.Context = controllers.NewAppContext()
	return a 
}

func (a *AppService) Run() {

	log.Println("AppService.Run()")
	if a.Context == nil {
                log.Fatal("AppService.Context == nil. Quitting\n")
	}
	a.Context.Run("8080")

}
