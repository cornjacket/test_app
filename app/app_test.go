package app

import (

        "fmt"
	"testing"
	"time"
	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/test_app/app/controllers"
	"bitbucket.org/cornjacket/test_app/client"
)

func NewAppServiceMock() AppService {
        a := AppService{}
        a.Context = NewAppContextMock()
        return a
}

func NewAppContextMock() *controllers.AppContext {
        context := controllers.AppContext{}
        return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct{
	AppService	AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppServiceMock()
        go s.AppService.Run()
	time.Sleep(1 * time.Second) // time delay to let server boot and talk to database
}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
}

func sendOnboardPacket(c *C, eui string) {
	// Send Packet
	fmt.Printf("sendOnboardPacket\n")

	c.Assert(1, Equals, 1)
}

func (s *TestSuite) TestPacketPath(c *C) {
	// Send Packet
	fmt.Printf("packetTest\n")

	c.Assert(1, Equals, 1)
}

func (s *TestSuite) TestClient(c *C) {
        // Send Packet
        fmt.Printf("clientTest\n")
        c.Assert(client.Return5(), Equals, 5)
}

