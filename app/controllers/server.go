package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type AppContext struct {
	Router *httprouter.Router
}

func NewAppContext() *AppContext {
	context := AppContext{}
	return &context
}

func (app *AppContext) Run(addr string) {
	// Instantiate a new router
        app.Router = httprouter.New()

	app.initializeRoutes()

	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}
