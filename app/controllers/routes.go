package controllers

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/context"
)

func (app *AppContext) initializeRoutes() {

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))

}

// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang 
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}
