module bitbucket.org/cornjacket/test_app

go 1.13

require (
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
)
