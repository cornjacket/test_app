package main

import (
	"fmt"
	"bitbucket.org/cornjacket/test_app/client"
	"bitbucket.org/cornjacket/test_app/app"
)

func main() {

	fmt.Printf("Test_app fmt inside main, %d\n", client.Return5())	
	client.Test()

	app := app.NewAppService()
	app.Run()

}
